import turtle


def draw_heart():
    turtle.speed(1)
    turtle.color('red')
    turtle.begin_fill()
    turtle.left(50)
    turtle.forward(133)
    turtle.circle(50, 200)

    # Dibujar el lado derecho del corazón
    turtle.right(140)
    turtle.circle(50, 200)
    turtle.forward(133)
    turtle.end_fill()


def main():
    turtle.setup(400, 400)
    turtle.title("Corazón")
    turtle.penup()
    turtle.goto(0, -150)
    turtle.pendown()

    draw_heart()

    turtle.done()


if __name__ == "__main__":
    main()

